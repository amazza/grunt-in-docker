FROM node:6.2.2
RUN apt-get update \
    && apt-get install -y \
    curl  && npm install -g grunt-cli

ENTRYPOINT [ "/usr/local/bin/grunt" ]

