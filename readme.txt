## Construir
docker build -t grunt .


## Ejemplo: si quiero usar el grunt solo para hacer el build
docker run --rm -v "$PWD":/usr/src/frontend -w /usr/src/frontend grunt build --prod
