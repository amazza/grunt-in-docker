
# Construir
    docker build -t grunt .

# Cambio de directorio a donde tengo el proyecto en el viejo, feo y deprecado angularJS
    cd /path/legacy/feo/project/angularjs

# Usar el grunt solo para hacer el build
    docker run --rm -v "$PWD":/usr/src/frontend -w /usr/src/frontend grunt build --prod

